<?php 

function CallAPI($URI, $data = false){

		    $ch = curl_init($URI);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

		    curl_setopt($ch, CURLOPT_POST, true);
		    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		    curl_setopt($ch, CURLOPT_URL, $URI);

		    $result = curl_exec($ch);
		    curl_close($ch);

		    return $result;
		}

function getPageTitle($_PAGE_TITLE){
	echo $_PAGE_TITLE;
}

function formatDate($date){
	$date = explode("T",$date);
	$date = implode('/', array_reverse(explode('-' ,$date[0])));
	return $date;
}

function formatCPF($cpf){
	$cpf = str_split($cpf);
	$cpf[0] = '*';
	$cpf[1] = '*';
	$cpf[2] = '*';
	$cpf[9] = '*';
	$cpf[10] = '*';
	$cpf = implode('', $cpf);
	return $cpf;
}