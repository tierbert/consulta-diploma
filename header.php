<!DOCTYPE html>
<html>
<head>
	<?php include 'functions.php' ?>
	<meta charset="utf-8">
	<title><?php getPageTitle($_PAGE_TITLE); ?></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/style-spec.css">
	<link rel="stylesheet" href="fontawesome/css/all.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Cinzel&display=swap" rel="stylesheet">
	<script src="fontawesome/js/all.js"></script>
</head>
<body>
	<header>
		<nav class="navbar navbar-light bg-blue-2 bd-shadow">
			<div class="container brand-div">
				<a href="#" class="navbar-brand"> <img class="brand-logo" src="img/unifg_logo_mono_h.png" alt="Centro universitário UNIFG"></a>
				<span class="header-title">Consulta de registro de diplomas</span>
			</div>
		</nav>
	</header>
	<main>
		<div class="container main-title">
			<h2>Digite o CPF do aluno no campo abaixo</h1>
		</div>

		<div class="container search-bar">
			<form method="POST" action="result.php">
				<div class="d-flex justify-content-center">
					<input onkeyup="mask('###.###.###-##',this,event,true)"type="text" name="cpfAluno" placeholder="Ex: 000.000.000-00" class="form-control col-sm-10 bd-radius" value="<?php echo isset($_POST["cpfAluno"]) ? $_POST['cpfAluno']:'' ?>">
					<button type="submit" class="btn button font-white col-sm-1  bd-radius"><i class="fas fa-search"></i></button>
				</div>
			</form>
		</div>
		
		<script>
			function mask(m,t,e,c){
				var cursor = t.selectionStart;
				var texto = t.value;
				texto = texto.replace(/\D/g,'');
				var l = texto.length;
				var lm = m.length;
				if(window.event) {                  
					id = e.keyCode;
				} else if(e.which){                 
					id = e.which;
				}
				cursorfixo=false;
				if(cursor < l)cursorfixo=true;
				var livre = false;
				if(id == 16 || id == 19 || (id >= 33 && id <= 40))livre = true;
				ii=0;
				mm=0;
				if(!livre){
					if(id!=8){
						t.value="";
						j=0;
						for(i=0;i<lm;i++){
							if(m.substr(i,1)=="#"){
								t.value+=texto.substr(j,1);
								j++;
							}else if(m.substr(i,1)!="#"){
								t.value+=m.substr(i,1);
							}
							if(id!=8 && !cursorfixo)cursor++;
							if((j)==l+1)break;
							
						} 	
					}
					if(c)coresMask(t);
				}
				if(cursorfixo && !livre)cursor--;
				t.setSelectionRange(cursor, cursor);
}
		</script>