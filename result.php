<?php $_PAGE_TITLE = "Listagem de Alunos" ?>
<?php include 'header.php' ?>
<?php 
	$resultMessage = '';
	$invalidCPFText = 'Digite um CPF válido';
	$student404Text = 'Não foi encontrado nenhum aluno com esse CPF.';


	$_POST['cpfAluno'] = str_replace('.', '', $_POST['cpfAluno'] );
	$_POST['cpfAluno'] = str_replace('-', '', $_POST['cpfAluno'] );

	if(isset($_POST['cpfAluno'])){
		if(sizeof(str_split($_POST['cpfAluno'], 1)) == 11){
			$postdata = (object) [
				'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
				'format' => 'json',
				'offset' => 0,
				'tipo' => 'BASICA',
				'filtrarPor' => 'CPF',
				'termo' => $_POST['cpfAluno']
			];
			$aluno = json_decode(CallAPI("https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/", $postdata));

			$registradora = array('id' => 1738, 'nome' => '2023 / Centro Universitário FG - UNIFG');

			if(isset($aluno[0]->idAluno)){
				$postdata = (object)[
					'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
					'format' => 'json',
					'offset' => 0,
					'idAluno' => $aluno[0]->idAluno,
				];
				$alunos = json_decode(CallAPI("https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/cursos-ingressos/listar/", $postdata));
				$postdata = (object)[
					'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
					'format' => 'json',
				];
				
				$cursos = json_decode(CallAPI("https://guanambi.jacad.com.br:443/academico/api/v1/academico/cursos-base/", $postdata));
				// var_dump($cursos[0]->nomeImpressao);
				$cursoAluno = array();

			} else {
				$resultMessage = $student404Text;
			}
		} else {
			$resultMessage = $invalidCPFText;
		}
	} else {
		$resultMessage = $invalidCPFText;
	}?>

		<div class="container main-table">
			<?php if(isset($alunos)){?>
				<table class="table table-hover">
					<thead>
						<th>Nome do aluno</th>
						<th>Curso</th>
					</thead>
					<tbody class="tbody">
						<?php foreach ($alunos as $aluno) { ?>
							<?php if($aluno->dataPublicacaoDiploma != '' && $aluno->idInstituicaoDiploma == $registradora['id'] && $aluno->idInstituicaoExpedidoraDiploma == $registradora['id']){ ?>
								<?php foreach ($cursos as $curso){
										if($aluno->_idCursoBase == $curso->idCursoBase){
											$cursoAluno = array('id' => $curso->idCursoBase, 'nome' => $curso->nomeImpressao, 'inep' => $curso->codigoInep);
										}
									}?>
								<tr class="table-row" data-toggle="modal" data-target="#exampleModalCenter">
									<td class="id-aluno"><?php echo $aluno->idAluno ?></td>
									<td class="row-aluno">
										<p><?php echo $aluno->_aluno ?></p>
										<span id="info-aluno-<?php echo $aluno->idAluno ?>" style="display:none;">
											<table class="table table-borderless" id="table-info">
												<tr>
													<th>Nome do Aluno: </th>
													<td><?php echo $aluno->_aluno ?></td>
												</tr>
												<tr>
													<th>CPF:</th>
													<td class="cpf"><?php echo $aluno->_cpf; ?></td>
												</tr>
												<tr>
													<th>Inep/Curso:</th>
													<td><?php echo $cursoAluno['inep'].' / '.$cursoAluno['nome']; ?></td>
												</tr>
												<tr>
													<th>Data Ingresso:</th>
													<td><?php echo formatDate($aluno->dataCadastro) ?></td>
												</tr>
												<tr>
													<th>Data de Conclusão do curso:</th>
													<td><?php echo formatDate($aluno->dataConclusao) ?></td>
												</tr>
												<tr>
													<th>Inep/IES Expedidora:</th>
													<td><?php echo $registradora['nome'] ?></td>
												</tr>
												<tr>
													<th>Inep/IES Registradora:</th>
													<td><?php echo $registradora['nome'] ?></td>
												</tr>		
												<tr>
													<th>Data de Expedição do Diploma:</th>
													<td><?php echo formatDate($aluno->dataExpedicaoDiploma) ?></td>
												</tr>	
												<tr>
													<th>Data de Registro do Diploma:</th>
													<td><?php echo formatDate($aluno->dataRegistro)?></td>
												</tr>
												<tr>
													<th>Registro:</th>
													<td>
														<div class="intern-table">
															<span>Livro:</span>
															<span><?php echo $aluno->livro?></span>
														</div>
														<div>
															<span>Número: </span>
															<span><?php echo $aluno->registro?></span>
														</div>
														<div>
															<span>Folha:</span>
															<span><?php echo $aluno->folha?></span>
														</div>
													</td>
												</tr>	
												<tr>
													<th>Publicação registro DOU:</th>
													<td><?php echo formatDate($aluno->dataPublicacaoDiploma)?></td>
												</tr>	
											</table>
										</span>
									</td>
									<td class="row-aluno"><p><?php echo $cursoAluno['nome']; ?></p></td>
								</tr>
								<?php } ?>
						<?php } ?>
					</tbody>
				</table>
				<?php } else {?>
					<div class=" justify-content-center">
						<div class="row">
							<div class="alert alert-danger container bd-radius col-sm-65">
								<?php echo $resultMessage; ?>
							</div>
						</div>
					</div>
				<?php }?>
		</div>
					
		<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Informações do aluno</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="modal-table">
						
					</div>
				</div>
			</div>
		</div>
	<script>
		//Váriavel que captura as linhas da tabela
		let rows = document.getElementsByClassName('table-row');

		//Função que percorre as linhas da tabela e adciona links para cada umda delas
		[].forEach.call(rows, (row) => { 
			row.addEventListener('click', () => {
				let infoAluno = document.getElementById("info-aluno-" + row.firstElementChild.textContent).firstElementChild;
				let clone = infoAluno.cloneNode(true);
				let tableModal = document.getElementById("modal-table");
				tableModal.innerHTML = "";
				tableModal.appendChild(clone);
			});
		});

		function formatCPF (cpf){
			cpf = cpf.split('');
			cpf.splice(3, 0, '.');
			cpf.splice(7, 0, '.');
			cpf.splice(11, 0, '-');
			cpf = cpf.join('');

			return cpf;
		}
		
		let cpf = document.querySelector('.cpf');
		cpf.textContent = formatCPF(cpf.textContent);
		
		</script>
<?php include 'footer.php' ?>