<?php $_PAGE_TITLE = "Dados do Aluno" ?>
<?php include 'header.php' ?>
		<?php 

			$postdata = (object) [
	    	'token' => '5e0d3b28bd55aa5bbb1d35ff89a08b93',
        	'format' => 'json',
        	'offset' => 0,
        	'idAluno' => $_GET['aluno'],
	 		];
			$aluno = json_decode (CallAPI("https://guanambi.jacad.com.br:443/academico/api/v1/academico/aluno/cursos-ingressos/listar/", $postdata))[0];

		?>
		<div class="container">
			<div class="row">
				Nome do Aluno: <?php echo $aluno->_aluno.''; ?>
			</div>
			<div class="row">
				CPF: <?php echo $aluno->_cpf.''; ?>
			</div>
			<div class="row">
				Curso: <?php echo $aluno->_curso.''; ?>
			</div>
			<div class="row">
				Data Ingresso: <?php echo $aluno->dataInicioCurso.''; ?>
			</div>
			<div class="row">
				Data de Conclusão do curso: <?php echo $aluno->dataConclusao.''; ?>
			</div>
			<div class="row">
				IES Expedidora: <?php echo 'Achar parâmetro'; ?>
			</div>
			<div class="row">
				IES Registrador: <?php echo 'Achar parâmetro'; ?>
			</div>
			<div class="row">
				Data de Expedição do Diploma: <?php echo $aluno->dataExpedicaoDiploma.''; ?>
			</div>
			<div class="row">
				Data de Registro do Diploma: <?php echo $aluno->dataRegistro.''; ?>
			</div>
			<div class="row">
				Expedição: <?php echo 'Achar parâmetro'; ?>
			</div>
			<div class="row">
				Publicação registro DOU: <?php echo 'Achar parâmetro'; ?>
			</div>
		</div>
		
<?php include 'footer.php' ?>